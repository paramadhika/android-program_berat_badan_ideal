import java.text.DecimalFormat
import java.util.Scanner

fun main(args: Array<String>) {
    val reader = Scanner(System.`in`)

    //    Jika ingin Mengulang output program tanpa harus memasukkan data berulang kali :D
    //    val nama: String ="I Made Paramadhika Dwi Putra"
    //    val jk: String = "L"
    //    val umur: Byte = 21
    //    val alamat: String = "Jalan Kori Nuansa Utama Selatan"
    //    val tinggicm: Int = 165
    //    val beratBadan: Double = 50.0

    //User Menginputkan Data Diri Sesuai dengan yang diminta program
    println("\t\t========= Input Data Diri =========")
    print("Masukkan Nama : ")
    val nama: String = reader.nextLine()

    print("Masukkan Jenis Kelamin (L/P) : ")
    val jk: String = reader.nextLine()

    print("Masukkan Usia : ")
    val umur: Byte = reader.nextByte()
    reader.nextLine()

    print("Masukkan Alamat : ")
    val alamat: String = reader.nextLine()

    print("Masukkan Tinggi : ")
    val tinggicm: Int = reader.nextInt()
    val tinggim: Float = (tinggicm / 100.0).toFloat()

    print("Masukkan Berat Badan (Kg) : ")
    val beratBadan: Double = reader.nextDouble()


    val dataDiri = """
        
             ========= Data Diri =========
        Nama 				: $nama
        Jenis Kelamin		: $jk
        Umur				: $umur
        Alamat				: $alamat
        Tinggi Badan (Cm)	: $tinggicm
        Berat Badan (Kg)	: $beratBadan
    """.trimIndent()

    println(dataDiri)

    //Pengecekan Umur sebelum menghitung BMI untuk menetukan user adalah seorang remaja/dewasa atau bukan
    val result = if (umur < 19)
        "Mohon Maaf, Anda belum termasuk dalam kriteria orang Remaja/Dewasa!"
        else{
            /*Klasifikasi BMI/IMT yang digunakan Pada perhitungan dibawah berdasarkan dari Depkes RI
            * secara umum untuk remaja*/
            val bmi = beratBadan / (tinggim*tinggim)    //Rumus Untuk Mengihitung Berat Massa Indeks (BMI)
            var format = DecimalFormat("0.##")   //Menyederhakan Angka dibelakang koma tidak lebih dari 2 angka
            //println(format.format(bmi))               //Digunakan untuk mencetak perhitungan BMI

            //Pengklasifikasian BMI
            if(bmi < 18.4)
                "Berat Badan Anda Menunjukan Anda Kekurangan Berat Badan"
            else if(bmi >= 18.5 && bmi <= 24.9)
                "Berat Badan Anda Normal!"
            else if(bmi >= 25 && bmi <= 26.9)
                "Berat Badan Anda Menunjukan Anda Memiliki Berat Badan Berlebih"
            else
                "Berat Badan Anda Menunjukan Bahwa Anda Mengalami Obesitas"
        }
        println(result)

    /*Fungsi yang digunakan untuk menghitung Berat Badan Ideal Berdasarkan Tinggi Badan
    Fungsi dibawah diambil dari website halodoc.com*/
    val ideal = if("$jk" == "L" || "$jk" == "l")        //Menghitung Berat Badan Ideal Pria
            ((tinggicm - 100) - ((tinggicm - 100)*0.1))
    else if ("$jk" == "P" || "$jk" == "p")              //Menghitung Berat Badan Ideal Perempuan
            ((tinggicm - 100) - ((tinggicm - 100)*0.15))
    else
        "!!!Terdapat Kesalahan Pada Data Diri : Jenis Kelamin (L/P)!!!"
    println("Berat Badan Ideal Anda berdasarkan Tinggi Badan anda adalah : $ideal")

    readLine()
    println("Terimakasih dan Mohon Feedback serta sarannya kak untuk tugas ini :D")
}